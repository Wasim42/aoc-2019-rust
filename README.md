# Advent of Code 2019 - in Rust

## A bit late?
Yes, it's almost December 2020 so obviously this isn't me being competitive!
Instead, it's an opportunity for me to learn Rust.

## How to run

This is a cargo binary.  This means that once you have rust installed, and
you've cloned this repo, from inside you can run:

```
cargo test
```

That will run all the unit tests.

You can run all the days with:

```
cargo run
```

To only run a day, you can use:

```
cargo run -- 3
```

And to run only, say, part 2 of a day:

```
cargo run -- 3 2
```


Note that by default the build is a debug build.  This means that it is a bit
slower than a release build.  For some of the days it can be _much_ slower!
To run a release build, use:

```
cargo run --release
```

Or even:

```
cargo run --release -- 3
```
