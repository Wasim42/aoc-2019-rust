mod input;

use petgraph::graph::Graph;
use petgraph::algo;

use std::collections::HashMap;

fn count_all_orbits(orbits: &str) -> i32 {
    let mut graph = Graph::new();

    let mut nodes: HashMap<&str, petgraph::prelude::NodeIndex> = HashMap::new();

    for connection in orbits.lines() {
        let mut names = connection.split(')');

        let name1 = names.next().unwrap();
        let node1;

        if !nodes.contains_key(name1) {
            node1 = graph.add_node(1.0);
            nodes.insert(name1, node1);
        } else {
            node1 = *nodes.get(name1).unwrap();
        }

        let name2 = names.next().unwrap();
        let node2;

        if !nodes.contains_key(name2) {
            node2 = graph.add_node(1.0);
            nodes.insert(name2, node2);
        } else {
            node2 = *nodes.get(name2).unwrap();
        }

        graph.add_edge(node1, node2, 1.0);
    }
    let common = *nodes.get("COM").unwrap();
    if let Ok((weights, _)) = algo::bellman_ford(&graph, common) {
        weights.iter().fold(0.0, |acc, x| acc + x) as i32
    } else {
        0
    }
}

fn count_orbital_transfers(orbits: &str) -> i32 {
    let mut graph = Graph::new_undirected();

    let mut nodes: HashMap<&str, petgraph::prelude::NodeIndex> = HashMap::new();

    for connection in orbits.lines() {
        let mut names = connection.split(')');

        let name1 = names.next().unwrap();
        let node1;

        if !nodes.contains_key(name1) {
            node1 = graph.add_node(1.0);
            nodes.insert(name1, node1);
        } else {
            node1 = *nodes.get(name1).unwrap();
        }

        let name2 = names.next().unwrap();
        let node2;

        if !nodes.contains_key(name2) {
            node2 = graph.add_node(1.0);
            nodes.insert(name2, node2);
        } else {
            node2 = *nodes.get(name2).unwrap();
        }

        graph.add_edge(node1, node2, 1.0);
    }
    let start = *nodes.get("YOU").unwrap();
    let answer = algo::dijkstra(&graph, start, None, |_| 1);
    // Remember to subtract 2 (YOU and SAN)
    answer.get(nodes.get("SAN").unwrap()).unwrap() - 2
}

pub fn part1() -> String {
    format!("{}", count_all_orbits(input::INPUT))
}

pub fn part2() -> String {
    format!("{}", count_orbital_transfers(input::INPUT))
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn check_all_orbits() {
        let test_data = r#"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L"#;
        assert_eq!(42, count_all_orbits(test_data));
    }

    #[test]
    fn check_orbital_transfer() {
        let test_data = r#"COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN"#;
        assert_eq!(4, count_orbital_transfers(test_data));
    }

}
