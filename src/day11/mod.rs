mod input;

use super::intcode;
use super::structs::{Location, Direction};
use super::structs;
use std::cmp;
use std::collections::HashMap;
use std::sync::mpsc;
use std::thread;

fn run_robot(panels: &mut HashMap<Location, i64>, init: i64) {
    let mut pos = Location{x: 0, y: 0};
    let mut dir = Direction::Up;
    panels.insert(pos.clone(), init);

    // Set up the intcode machine's input & output, then let it go!
    let mut machine = intcode::Machine::new(input::INPUT);
    let inputter = machine.get_inputter();
    let (tx, rx) = mpsc::channel::<i64>();
    machine.set_outputter(tx);
    let t = thread::spawn(move || {machine.run()});

    // At the first position there is just black - so we input 0
    inputter.send(init).unwrap();

    // Now the main loop
    while let Ok(colour) = rx.recv() {
        panels.insert(pos.clone(), colour);
        let rotation = rx.recv().unwrap();
        if rotation == 1 {
            dir = structs::turn_right(&dir);
        } else {
            dir = structs::turn_left(&dir);
        }
        pos = pos.turn(&dir);
        // Now to feed back the colour of this panel
        inputter.send(*panels.get(&pos).unwrap_or(&0)).unwrap();
    }
    t.join().unwrap();
 }

pub fn part1() -> String {
    // Set up our panels for painting
    let mut panels = HashMap::new();

    run_robot(&mut panels, 0);

    format!("{}", panels.len())
}

pub fn part2() -> String {
    // Set up our panels for painting
    let mut panels = HashMap::new();

    run_robot(&mut panels, 1);

    // Get the min/max x and y of panels so we can set boundaries
    let mut minx = 0;
    let mut maxx = 0;
    let mut miny = 0;
    let mut maxy = 0;

    for pos in panels.keys() {
        minx = cmp::min(minx, pos.x);
        maxx = cmp::max(maxx, pos.x);
        miny = cmp::min(miny, pos.y);
        maxy = cmp::max(maxy, pos.y);
    }
    println!("({},{}) -> ({},{})", minx, miny, maxx, maxy);
    let mut label = String::new();
    for y in miny..maxy+1 {
        let mut line = String::with_capacity((maxx - minx + 1) as usize);
        for x in minx..maxx+1 {
            let colour = panels.get(&Location{x,y}).unwrap_or(&0);
            if colour == &1 {
                line += "#";
            } else {
                line += " ";
            }
        }
        line += "\n";
        label += &line;
    }

    format!("\n{}", label)
}
