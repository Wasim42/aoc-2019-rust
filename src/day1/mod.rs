// Day 1 - and so we begin

mod input;

fn input_to_i32() -> Vec<i32> {
    input::INPUT.lines().map(|x| x.parse::<i32>().unwrap()).collect()
}

fn get_fuel(m: &i32) -> i32 {
    m / 3 - 2
}

pub fn part1() -> String {
    let nums = input_to_i32();
    let answer = nums.iter().fold(0, |acc, x| acc + get_fuel(x));
    format!("{}", answer)
}

pub fn part2() -> String {
    let nums = input_to_i32();
    let mut answer = 0;
    for x in nums {
        let mut fuel = get_fuel(&x);
        while fuel > 0 {
            answer += fuel;
            fuel = get_fuel(&fuel);
        }
    }
    format!("{}", answer)
}
