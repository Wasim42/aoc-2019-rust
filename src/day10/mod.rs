mod input;
use super::structs::Location;


use std::collections::{HashSet, HashMap};

fn parse_astfield(astfield: &str) -> HashSet<Location> {
    let mut asteroids = HashSet::new();

    for (y, line) in astfield.lines().enumerate() {
        let line = line.trim();
        if line.is_empty() {
            continue;
        }
        for (x, pos) in line.chars().enumerate() {
            if pos == '#' {
                asteroids.insert(Location{x: x as i32, y: y as i32});
            }
        }
    }

    asteroids
}

fn get_visibles(asteroids: &HashSet<Location>, starter: &Location) -> Vec<Location> {

    let mut found = Vec::new();

    for destination in asteroids {
        if starter == destination {
            // Don't care about ourselves
            continue;
        }
        let grad = starter.get_gradient(destination);

        // We now start from starter, and keep looping by grad until we
        // get another asteroid - if it's destination then we can add a
        // counter to the starter, otherwise we ignore it.
        let mut pos = starter.clone();
        loop {
            pos = pos.apply_grad(grad);
            if asteroids.contains(&pos) {
                break;
            }
        }
        if &pos == destination {
            // This destination is visible!
            found.push(pos);
        }
    }

    found
}

fn display_field(astfield: &[Location], starter: &Location) {
    // Step 1 - find the limits
    let mut max_x = starter.x;
    let mut max_y = starter.y;

    // Modify this as required
    //let astfield = astfield[195..205].iter().collect::<Vec<&Location>>();

    // We'll convert the astfield into a HashSet to make the display much
    // easier
    let mut set = HashMap::new();

    for (pos, a) in astfield.iter().enumerate() {
        if a.x > max_x {
            max_x = a.x;
        }
        if a.y > max_y {
            max_y = a.y;
        }
        set.insert(Location{x:a.x, y:a.y}, pos+1);
    }

    for y in 0..max_y+1 {
        let mut line = String::new();
        for x in 0..max_x+1 {
            if x == starter.x && y == starter.y {
                line.push('^');
            } else if let Some(pos) = set.get(&Location{x,y}) {
                let mut disp = b'0' + (*pos as u8)%55;
                if disp > 57 {
                    disp += 7;
                }
                if disp > 90 {
                    disp += 7;
                }
                line.push(disp as char);
            } else {
                line.push('.');
            }
        }
        println!("{}", line);
    }
}

fn solve_part1(astfield: &str) -> i32 {
    let asteroids = parse_astfield(astfield);

    // From each asteroid, get the gradient to each other asteroid, then for
    // each point in the line, between the two end-points, see if another
    // asteroid exists there.  If none do, then add to the count for that
    // position.
    let mut visible = 0;

    for starter in &asteroids {

        let count = get_visibles(&asteroids, starter).len() as i32;
        if count > visible {
            visible = count;
        }
    }
    visible
}

fn solve_part2(astfield: &str, wanted_laser: usize, debug: bool) -> i32 {
    // Step 1 - we need to find the location of our laser canon
    let mut asteroids = parse_astfield(astfield);

    // From each asteroid, get the gradient to each other asteroid, then for
    // each point in the line, between the two end-points, see if another
    // asteroid exists there.  If none do, then add to the count for that
    // position.
    let mut visible = 0;

    let mut starter = Location{x: 0, y: 0};

    for victim in &asteroids {

        let count = get_visibles(&asteroids, victim).len() as i32;
        if count > visible {
            visible = count;
            starter = victim.clone();
        }
    }

    // Now that we have our starter, we can remove it from the asteroid list
    asteroids.remove(&starter);

    let mut lasered = wanted_laser;

    // Step 2 - keep lasering away the asteroids until we're at the last few
    // that matter
    let mut to_remove;

    loop {
        to_remove = get_visibles(&asteroids, &starter);
        let number = to_remove.len();
        if number >= lasered {
            break;
        }
        for i in to_remove {
            asteroids.remove(&i);
            lasered -= 1;
        }
    }

    // Now we need to go clockwise through to_remove, removing them until we
    // know the 200th one
    to_remove.sort_unstable_by(|a,b| starter.get_angle(&a).partial_cmp(&starter.get_angle(&b)).unwrap());

    if debug {
        display_field(&to_remove, &starter);
    }
    
    /*
    for (i, j) in to_remove.iter().enumerate() {
        println!("{} ({}, {})", i + wanted_laser - lasered + 1, j.x, j.y);
    }
    */

    let the_one = &to_remove[lasered - 1]; // We start at zero!
    the_one.x*100 + the_one.y
}

pub fn part1() -> String {
    let answer = solve_part1(input::INPUT);
    format!("{}", answer)
}

pub fn part2() -> String {
    let answer = solve_part2(input::INPUT, 200, false);
    format!("{}", answer)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let model_answers = vec![
            (r#".#..#
            .....
            #####
            ....#
            ...##"#, 8),
            (r#"......#.#.
            #..#.#....
            ..#######.
            .#.#.###..
            .#..#.....
            ..#....#.#
            #..#....#.
            .##.#..###
            ##...#..#.
            .#....####"#,33),
            (r#"#.#...#.#.
            .###....#.
            .#....#...
            ##.#.#.#.#
            ....#.#.#.
            .##..###.#
            ..#...##..
            ..##....##
            ......#...
            .####.###."#, 35),
            (r#".#..#..###
            ####.###.#
            ....###.#.
            ..###.##.#
            ##.##.#.#.
            ....###..#
            ..#.#..#.#
            #..#.#.###
            .##...##.#
            .....#.#.."#, 41),
            (r#".#..##.###...#######
            ##.############..##.
            .#.######.########.#
            .###.#######.####.#.
            #####.##.#.##.###.##
            ..#####..#.#########
            ####################
            #.####....###.#.#.##
            ##.#################
            #####.##.###..####..
            ..######..##.#######
            ####.##.####...##..#
            .#####..#.######.###
            ##...#.##########...
            #.##########.#######
            .####.#.###.###.#.##
            ....##.##.###..#####
            .#.#.###########.###
            #.#.#.#####.####.###
            ###.##.####.##.#..##"#, 210),
        ];

        for mas in model_answers {
            let question = mas.0;
            let answer = mas.1;
            assert_eq!(answer, solve_part1(question));
        }
    }

    #[test]
    fn test_part2_my() {
        let astfield = r#"
        ...................
        ...................
        ..################.
        ..################.
        ..##............##.
        ..##............##.
        ..##............##.
        ..##............##.
        ..##............##.
        ..##............##.
        ..##.....#......##.
        ..##............##.
        ..##............##.
        ..##............##.
        ..##............##.
        ..##............##.
        ..##............##.
        ..################.
        ..################.
        ...................
        ..................."#;
        let answer = 1706;
        assert_eq!(answer, solve_part2(astfield, 20, true));
    }

    #[test]
    fn test_part2() {
        let astfield = r#".#..##.###...#######
        ##.############..##.
        .#.######.########.#
        .###.#######.####.#.
        #####.##.#.##.###.##
        ..#####..#.#########
        ####################
        #.####....###.#.#.##
        ##.#################
        #####.##.###..####..
        ..######..##.#######
        ####.##.####...##..#
        .#####..#.######.###
        ##...#.##########...
        #.##########.#######
        .####.#.###.###.#.##
        ....##.##.###..#####
        .#.#.###########.###
        #.#.#.#####.####.###
        ###.##.####.##.#..##"#;
        let answer = 802;

        assert_eq!(answer, solve_part2(astfield, 200, false));
    }

}
