mod input;

use super::intcode;
use std::sync::mpsc;
use std::thread;

pub fn part1() -> String {
    let mut machine = intcode::Machine::new(input::INPUT);
    let inputter = machine.get_inputter();
    inputter.send(1).unwrap();
    let (tx, rx) = mpsc::channel::<i64>();
    machine.set_outputter(tx);
    let t = thread::spawn(move || {machine.run()});
    let mut result = 99;
    for check in rx {
        result = check;
    }
    t.join().unwrap();
    format!("{}", result)
}

pub fn part2() -> String {
    let mut machine = intcode::Machine::new(input::INPUT);
    let inputter = machine.get_inputter();
    inputter.send(2).unwrap();
    let (tx, rx) = mpsc::channel::<i64>();
    machine.set_outputter(tx);
    let t = thread::spawn(move || {machine.run()});
    let mut result = 99;
    for check in rx {
        result = check;
    }
    t.join().unwrap();
    format!("{}", result)
}
