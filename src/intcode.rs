// I know that Intcode will eventually grow to something really quite
// magnificent!  For now it's very basic but it would be good to have a
// reasonable interface.

use std::collections::HashMap;
use std::sync::{Arc, mpsc, Mutex};
use std::thread;
use std::time::Duration;



#[derive(Debug)]
pub struct Machine {
    pub name: String,
    memory: HashMap<i64, i64>,
    pc: i64,
    diagnostics: bool,
    // The channels can be confusing, so here's a rundown.
    // rx is the receiving channel - through which we get our inputs
    // inputter is the sender that's cloned to whatever that wants to send us
    // a message.  It's the other end of the rx
    // tx is the transmitter that's been set by whatever wants our output.
    tx: Option<Arc<Mutex<mpsc::Sender<i64>>>>,
    inputter: Arc<Mutex<mpsc::Sender<i64>>>,
    rx: Arc<Mutex<mpsc::Receiver<i64>>>,
    output: i64,
    rel_base: i64,
}

impl Machine {
    pub fn new(program: &str) -> Machine {
        let mut pos = -1;
        // contents will be a Vector [(0, 3), (1, 1), (2, 4)…]
        let contents = program.split(',')
            .map(|x| {
                pos += 1;
                (pos, x.parse::<i64>().unwrap())
            })
        .collect::<Vec<_>>();

        let (uinputter, urx) = mpsc::channel::<i64>();
        let rx = Arc::new(Mutex::new(urx));
        let inputter = Arc::new(Mutex::new(uinputter));
        Machine {
            name: "unnamed".to_string(),
            memory: contents.iter().cloned().collect(),
            pc: 0,
            diagnostics: false,
            tx: None,
            inputter,
            rx,
            output: 0,
            rel_base: 0,
        }
    }

    pub fn set_name(&mut self, name: String) {
        self.name = name;
    }

    #[allow(dead_code)]
    pub fn diag(&mut self, set: bool) {
        self.diagnostics = set;
    }

    pub fn peek(&self, pos: i64) -> Option<&i64> {
        self.memory.get(&pos)
    }

    pub fn poke(&mut self, pos: i64, value: i64) {
        self.memory.insert(pos, value);
    }

    // This will return something through which you can send input to the
    // machine
    pub fn get_inputter(&mut self) -> mpsc::Sender<i64> {
        mpsc::Sender::clone(&self.inputter.lock().unwrap())
    }

    pub fn get_output(&mut self) -> i64 {
        self.output
    }

    pub fn set_outputter(&mut self, sender: mpsc::Sender<i64>) {
        self.tx = Some(Arc::new(Mutex::new(sender)));
    }

}

macro_rules! read_mem {
    ($mem:expr, $rel_base:expr, $bflag:expr, $param:expr, $pc:expr) => {
        {
            let mode = $bflag / (10 as i64).pow($param) % 10;
            let posnum = $mem.get(($pc)).unwrap_or(&0).clone();
            match mode {
                0 => $mem.get(&posnum).unwrap_or(&0).clone(),
                1 => posnum,
                2 => $mem.get(&(posnum + $rel_base)).unwrap_or(&0).clone(),
                _ => panic!("Invalid mode {}", mode),
            }
        }
    }
}

macro_rules! write_mem {
    ($mem:expr, $rel_base:expr, $bflag:expr, $param:expr, $pc:expr, $val:expr) => {
        {
            let mode = $bflag / (10 as i64).pow($param) % 10;
            let posnum = $mem.get(($pc)).unwrap_or(&0).clone();
            match mode {
                0 => $mem.insert(posnum, $val),
                2 => $mem.insert((posnum + $rel_base), $val),
                _ => panic!("Invalid mode {}", mode),
            }
        }
    }
}

impl Machine {
    pub fn run(&mut self) {
        loop {
            let op = *self.memory.get(&self.pc).unwrap();
            if self.diagnostics {
                println!("Op: {}", op);
            }
            match op%100 {
                99 => return,
                1 => self.pc = self.add(op, self.pc),
                2 => self.pc = self.multiply(op, self.pc),
                3 => self.pc = self.input(op, self.pc),
                4 => self.pc = self.output(op, self.pc),
                5 => self.pc = self.jump_if_true(op, self.pc),
                6 => self.pc = self.jump_if_false(op, self.pc),
                7 => self.pc = self.less_than(op, self.pc),
                8 => self.pc = self.equals(op, self.pc),
                9 => self.pc = self.relbase(op, self.pc),
                _ => panic!("Invalid Opcode: {}", op),
            }
        }
    }
    fn add(&mut self, op: i64, pc: i64) -> i64 {
        let mut pos = pc+1;
        let bitflags = (op - op%100)/100;
        let num1 = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        pos += 1;
        let num2 = read_mem!(self.memory, self.rel_base, bitflags, 1, &pos);
        let res = num1+num2;
        pos += 1;
        write_mem!(self.memory, self.rel_base, bitflags, 2, &pos, res);
        pos + 1
    }
    fn multiply(&mut self, op: i64, pc: i64) -> i64 {
        let mut pos = pc+1;
        let bitflags = (op - op%100)/100;
        let num1 = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        pos += 1;
        let num2 = read_mem!(self.memory, self.rel_base, bitflags, 1, &pos);
        let res = num1*num2;
        pos += 1;
        write_mem!(self.memory, self.rel_base, bitflags, 2, &pos, res);
        pos + 1
    }
    fn input(&mut self, op: i64, pc: i64) -> i64 {
        let pos = pc+1;
        let bitflags = (op - op%100)/100;
        let mut wait = 0;
        loop {
            if let Ok(val) = self.rx.lock().unwrap().recv() {
                write_mem!(self.memory, self.rel_base, bitflags, 0, &pos, val);
                return pos + 1
            } else {
                thread::sleep(Duration::from_millis(100));
                wait += 1;
            }
            if wait > 10 {
                panic!("[{}] Unable to receive input!", self.name);
            }
        }
    }
    fn output(&mut self, op: i64, pc: i64) -> i64 {
        let pos = pc+1;
        let bitflags = (op - op%100)/100;
        let val = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        if self.diagnostics {
            println!("***Output: {}", val);
        }
        self.output = val;
        // If we fail to send the output it doesn't matter
        if let Some(ltx) = &self.tx {
            // We ignore the error - should we?
            let _ = ltx.lock().unwrap().send(val);
        }
        pos + 1
    }
    fn jump_if_true(&mut self, op: i64, pc: i64) -> i64 {
        let mut pos = pc+1;
        let bitflags = (op - op%100)/100;
        let check = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        pos += 1;
        let newpos = read_mem!(self.memory, self.rel_base, bitflags, 1, &pos);
        if check != 0 {
            newpos
        } else {
            pos + 1
        }
    }
    fn jump_if_false(&mut self, op: i64, pc: i64) -> i64 {
        let mut pos = pc+1;
        let bitflags = (op - op%100)/100;
        let check = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        pos += 1;
        let newpos = read_mem!(self.memory, self.rel_base, bitflags, 1, &pos);
        if check == 0 {
            newpos
        } else {
            pos + 1
        }
    }
    fn less_than(&mut self, op: i64, pc: i64) -> i64 {
        let mut pos = pc+1;
        let bitflags = (op - op%100)/100;
        let p1 = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        pos += 1;
        let p2 = read_mem!(self.memory, self.rel_base, bitflags, 1, &pos);
        pos += 1;
        let answer;
        if p1 < p2 {
            answer = 1;
        } else {
            answer = 0;
        }
        write_mem!(self.memory, self.rel_base, bitflags, 2, &pos, answer);
        pos + 1
    }
    fn equals(&mut self, op: i64, pc: i64) -> i64 {
        let mut pos = pc+1;
        let bitflags = (op - op%100)/100;
        let p1 = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        pos += 1;
        let p2 = read_mem!(self.memory, self.rel_base, bitflags, 1, &pos);
        pos += 1;
        let answer;
        if p1 == p2 {
            answer = 1;
        } else {
            answer = 0;
        }
        write_mem!(self.memory, self.rel_base, bitflags, 2, &pos, answer);
        pos + 1
    }
    fn relbase(&mut self, op: i64, pc: i64) -> i64 {
        let pos = pc+1;
        let bitflags = (op - op%100)/100;
        let p1 = read_mem!(self.memory, self.rel_base, bitflags, 0, &pos);
        self.rel_base += p1;
        pos + 1
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn first_add_test() {
        let input = "1,9,10,3,2,3,11,0,99,30,40,50";
        let mut machine = Machine::new(input);
        machine.run();
        let answer = machine.peek(0).unwrap();
        assert_eq!(3500, *answer);
    }

    #[test]
    fn mode_multiply_test() {
        let input = "1002,4,3,4,33";
        let mut machine = Machine::new(input);
        machine.run();
        let answer = machine.peek(4).unwrap();
        assert_eq!(99, *answer);
    }

    #[test]
    fn mode_inout_test() {
        let input = "3,0,4,0,99";
        let mut machine = Machine::new(input);
        let testval = 42;
        let inputter = machine.get_inputter();
        inputter.send(testval).unwrap();
        machine.run();
        let answer = machine.get_output();
        assert_eq!(testval, answer);
    }

    #[test]
    fn pos_equals_8_test() {
        let input = "3,9,8,9,10,9,4,9,99,-1,8";
        let test_data = vec![(0, 0), (8, 1), (15, 0)];
        for check in test_data {
            let mut machine = Machine::new(input);
            let inputter = machine.get_inputter();
            inputter.send(check.0).unwrap();
            machine.run();
            assert_eq!(check.1, machine.get_output());
        }
    }

    #[test]
    fn pos_less_than_8_test() {
        let input = "3,9,7,9,10,9,4,9,99,-1,8";
        let test_data = vec![(0, 1), (8, 0), (15, 0)];
        for check in test_data {
            let mut machine = Machine::new(input);
            let inputter = machine.get_inputter();
            inputter.send(check.0).unwrap();
            machine.run();
            assert_eq!(check.1, machine.get_output());
        }
    }

    #[test]
    fn imm_equals_8_test() {
        let input = "3,3,1108,-1,8,3,4,3,99";
        let test_data = vec![(0, 0), (8, 1), (15, 0)];
        for check in test_data {
            let mut machine = Machine::new(input);
            let inputter = machine.get_inputter();
            inputter.send(check.0).unwrap();
            machine.run();
            assert_eq!(check.1, machine.get_output());
        }
    }

    #[test]
    fn imm_less_than_8_test() {
        let input = "3,3,1107,-1,8,3,4,3,99";
        let test_data = vec![(0, 1), (8, 0), (15, 0)];
        for check in test_data {
            let mut machine = Machine::new(input);
            let inputter = machine.get_inputter();
            inputter.send(check.0).unwrap();
            machine.run();
            assert_eq!(check.1, machine.get_output());
        }
    }

    #[test]
    fn pos_jump_test() {
        let input = "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9";
        let test_data = vec![(0, 0), (8, 1), (15, 1)];
        for check in test_data {
            let mut machine = Machine::new(input);
            let inputter = machine.get_inputter();
            inputter.send(check.0).unwrap();
            machine.run();
            assert_eq!(check.1, machine.get_output());
        }
    }

    #[test]
    fn imm_jump_test() {
        let input = "3,3,1105,-1,9,1101,0,0,12,4,12,99,1";
        let test_data = vec![(0, 0), (8, 1), (15, 1)];
        for check in test_data {
            let mut machine = Machine::new(input);
            let inputter = machine.get_inputter();
            inputter.send(check.0).unwrap();
            machine.run();
            assert_eq!(check.1, machine.get_output());
        }
    }

    #[test]
    fn bigger_compare_test() {
        let input = "3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99";
        let test_data = vec![(0, 999), (8, 1000), (15, 1001)];
        for check in test_data {
            let mut machine = Machine::new(input);
            let inputter = machine.get_inputter();
            inputter.send(check.0).unwrap();
            machine.run();
            assert_eq!(check.1, machine.get_output());
        }
    }

    #[test]
    fn check_day9() {
        let test_data = [
            ("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99",
             "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"),
             ("1102,34915192,34915192,7,4,7,99,0", "1219070632396864"),
             ("104,1125899906842624,99", "1125899906842624"),
        ];

        for (prog, answer) in test_data.iter() {
            let mut machine = Machine::new(prog);
            let (tx, rx) = mpsc::channel::<i64>();
            machine.set_outputter(tx);
            let _ = thread::spawn(move || {machine.run()});
            let mut guess = String::new();
            for i in rx {
                if guess.len() > 0 {
                    guess = format!("{},{}", guess, i);
                } else {
                    guess = format!("{}", i);
                }
            }
            assert_eq!(answer.to_string(), guess);
        }
    }
}
