use super::algorithms;

pub enum Direction {
    Up,
    Right,
    Down,
    Left,
}

pub fn turn_right(start: &Direction) -> Direction {
    match start {
        Direction::Up => Direction::Right,
        Direction::Right => Direction::Down,
        Direction::Down => Direction::Left,
        Direction::Left => Direction::Up,
    }
}

pub fn turn_left(start: &Direction) -> Direction {
    match start {
        Direction::Up => Direction::Left,
        Direction::Right => Direction::Up,
        Direction::Down => Direction::Right,
        Direction::Left => Direction::Down,
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Location {
    pub x: i32,
    pub y: i32,
}

impl Location {

    pub fn turn(&self, dir: &Direction) -> Location {
        let mut pos = Location{x: self.x, y: self.y};
        match dir {
            Direction::Up => pos.y -= 1,
            Direction::Right => pos.x += 1,
            Direction::Left => pos.x -= 1,
            Direction::Down => pos.y += 1,
        }
        pos
    }

    pub fn get_gradient(&self, other: &Location) -> (i32, i32) {
        let xs = other.x - self.x;
        let ys = other.y - self.y;
        // Handle straight up or across
        if xs == 0 {
            if ys > 0 {
                return (0, 1);
            }
            return (0, -1);
        }
        if ys == 0 {
            if xs > 0 {
                return (1, 0);
            }
            return (-1, 0);
        }
        // It's a standard gradient, do the fraction thing
        let gcd = algorithms::gcdi(xs, ys);
        (xs/gcd, ys/gcd)
    }
    pub fn apply_grad(&self, grad: (i32, i32)) -> Location {
        Location{
            x: self.x + grad.0,
            y: self.y + grad.1,
        }
    }
    pub fn get_angle(&self, other: &Location) -> f64 {
        // atan2 converts from cartesian to polar co-ordinates
        // Unfortunately the instructions we have require us to use a
        // clockwise rotation, starting at 12 o'clock
        // atan2 starts at 3 o'clock and rotates anti-clockwise.
        // Also our 'y' values are backwards.  Up is negative y, but
        // right is positive x
        // atan2 assumes up is positive y, and right is positive x.
        //
        // To get this right, we'll flip the y values, and subtract the
        // angle from pi/2
        
        let xs = (self.x - other.x) as f64;
        let ys = (self.y - other.y) as f64;

        let mut angle = ys.atan2(xs) - std::f64::consts::PI/2.0;
        if angle < 0.0 {
            angle += std::f64::consts::PI * 2.0;
        }
        angle
    }
}

