pub fn gcdi(m: i32, n: i32) -> i32 {
    let m = m.abs();
    let n = n.abs();
    match n {
        0 => m,
        _ => gcdi(n, m%n),
    }
}
