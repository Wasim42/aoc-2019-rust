const INPUT: &str = "347312-805915";

fn parse_input(i: &str) -> (i32, i32) {
    let mut numbers = i.split('-');

    let num1 = numbers.next().unwrap().parse::<i32>().unwrap();
    let num2 = numbers.next().unwrap().parse::<i32>().unwrap();

    (num1, num2)
}

fn meets_match1(number: String) -> bool {
    let mut found_double = false;
    let mut last = ' ';
    for i in number.chars() {
        if i < last {
            return false;
        }
        if i == last {
            found_double = true;
        }
        last = i;
    }
    found_double
}

fn meets_match2(number: String) -> bool {
    let mut found_double = false;
    let mut last = ' ';
    let mut repeat = 0;
    for i in number.chars() {
        if i < last {
            return false;
        }
        if i == last {
            repeat += 1;
        } else {
            if repeat == 1 {
                found_double = true;
            }
            repeat = 0;
        }
        last = i;
    }
    if repeat == 1 {
        found_double = true;
    }
    found_double
}

pub fn part1() -> String {
    let (low, high) = parse_input(INPUT);

    let mut count = 0;
    for i in low..high+1 {
        let num = format!("{}", i);
        if meets_match1(num) { count += 1 }
    }
    format!("{}", count)
}

pub fn part2() -> String {
    let (low, high) = parse_input(INPUT);

    let mut count = 0;
    for i in low..high+1 {
        let num = format!("{}", i);
        if meets_match2(num) { count += 1 }
    }
    format!("{}", count)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d4_part1() {
        assert_eq!(true, meets_match1("111111".to_string()));
        assert_eq!(false, meets_match1("223450".to_string()));
        assert_eq!(false, meets_match1("123789".to_string()));
    }

    #[test]
    fn test_d4_part2() {
        assert_eq!(true, meets_match2("112233".to_string()));
        assert_eq!(false, meets_match2("123444".to_string()));
        assert_eq!(true, meets_match2("111122".to_string()));
    }
}
