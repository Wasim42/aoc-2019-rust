mod algorithms;
mod structs;
mod intcode;
mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;

use std::env;


macro_rules! add_day {
    ($day:ident) => {
        [$day::part1, $day::part2]
    }
}

macro_rules! do_part {
    ($days:expr, $day:expr, $part:expr) => {
        println!("Day {} part {}: {}", $day, $part, $days[$day-1][$part-1]());
    }
}

fn help(name: &str) {
    println!("{} <day> <part>", name);
    println!("\n  Running just {} will run all the days and parts", name);
}

fn main() {

    let args: Vec<_> = env::args().collect();

    if args.len() > 3 {
        help(&args[0]);
        return;
    }

    // Fill in the days we know about
    let days = vec![
        add_day!(day1),
        add_day!(day2),
        add_day!(day3),
        add_day!(day4),
        add_day!(day5),
        add_day!(day6),
        add_day!(day7),
        add_day!(day8),
        add_day!(day9),
        add_day!(day10),
        add_day!(day11),
    ];

    // How this will work:
    // Running just aoc2019 will run through all the days
    // Running "aoc2019 1" will run both parts of day 1
    // Running "aoc2019 1 1" will run just day 1 part 1
    // By default we run all the days
    if args.len() == 1 {
        for day in 0..days.len() {
            do_part!(days, day+1, 1);
            do_part!(days, day+1, 2);
        }
        return;
    }

    let day: usize;
    if let Ok(daynum) = args[1].parse::<usize>() {
        if daynum > 0 && daynum <= days.len() {
            day = daynum;
        } else {
            println!("Invalid day {}", daynum);
            println!("Days between 1 and {} only", days.len());
            return;
        }
    } else {
        println!("Can't parse {} as a day", args[1]);
        help(&args[0]);
        return;
    }
    // We have our day, do we have any parts?
    if args.len() == 2 {
        // Only the day specified, do both parts
        do_part!(days, day, 1);
        do_part!(days, day, 2);
        return;
    }
    if args[2] == "1" {
        do_part!(days, day, 1);
    } else if args[2] == "2" {
        do_part!(days, day, 2);
    } else {
        println!("Only parts 1 and 2 are valid");
        help(&args[0]);
    }
}
