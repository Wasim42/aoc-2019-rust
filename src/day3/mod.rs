mod input;

use std::collections::{HashSet, HashMap};

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Point {
    x: i32,
    y: i32,
}

impl Point {
    fn up(&self, amount: i32) -> Vec<Point> {
        let mut positions = Vec::new();
        for i in 1..amount+1 {
            positions.push(Point{x: self.x, y: self.y+i});
        }
        positions
    }
    fn down(&self, amount: i32) -> Vec<Point> {
        let mut positions = Vec::new();
        for i in 1..amount+1 {
            positions.push(Point{x: self.x, y: self.y-i});
        }
        positions
    }
    fn left(&self, amount: i32) -> Vec<Point> {
        let mut positions = Vec::new();
        for i in 1..amount+1 {
            positions.push(Point{x: self.x-i, y: self.y});
        }
        positions
    }
    fn right(&self, amount: i32) -> Vec<Point> {
        let mut positions = Vec::new();
        for i in 1..amount+1 {
            positions.push(Point{x: self.x+i, y: self.y});
        }
        positions
    }
}

fn parse_line(readme: &str) -> HashSet<Point> {
    // A line is a set of comma-separated motions, originating at (0,0)
    let origin = Point{x: 0, y: 0};

    let mut positions = HashSet::new();

    let mut p = origin;

    for motion in readme.split(',') {
        let (dir, amount) = motion.split_at(1);
        let amount = amount.parse::<i32>().unwrap();
        let mut newp = Point{x:0, y:0};
        match dir {
            "U" => for pos in p.up(amount)    { newp = pos.clone(); positions.insert(pos); },
            "D" => for pos in p.down(amount)  { newp = pos.clone(); positions.insert(pos); },
            "L" => for pos in p.left(amount)  { newp = pos.clone(); positions.insert(pos); },
            "R" => for pos in p.right(amount) { newp = pos.clone(); positions.insert(pos); },
            _ => panic!("Unexpected direction {}", dir),
        }
        p = Point{x: newp.x, y: newp.y};

    }
    positions
}

// Given the set of directions, this will return a mapping of all the points
// that are hit, and the shortest number of moves it took to get to that
// point.
fn parse_line2(readme: &str) -> HashMap<Point, i32> {
    // A line is a set of comma-separated motions, originating at (0,0)
    let mut p = Point{x: 0, y: 0};
    let mut travel = 0;

    let mut positions = HashMap::new();

    for motion in readme.split(',') {
        let (dir, amount) = motion.split_at(1);
        let amount = amount.parse::<i32>().unwrap();
        for _ in 0..amount {
            travel += 1;
            let p2: Point;
            match dir {
                "U" => p2 = Point{y: p.y + 1, ..p},
                "D" => p2 = Point{y: p.y - 1, ..p},
                "L" => p2 = Point{x: p.x - 1, ..p},
                "R" => p2 = Point{x: p.x + 1, ..p},
                _ => panic!("Unexpected direction {}", dir),
            }
            p = p2.clone();
            positions.entry(p2).or_insert(travel);
        }
    }
    positions
}

fn parse_input(readme: &str) -> (HashSet<Point>, HashSet<Point>) {
    // The input consists of two lines, each a comma-separated list of motions
    let mut lines = readme.lines();
    let line1 = lines.next().unwrap();
    let line2 = lines.next().unwrap();

    let points1 = parse_line(line1);
    let points2 = parse_line(line2);

    (points1, points2)
}

fn parse_input2(readme: &str) -> (HashMap<Point, i32>, HashMap<Point, i32>) {
    // The input consists of two lines, each a comma-separated list of motions
    let mut lines = readme.lines();
    let line1 = lines.next().unwrap();
    let line2 = lines.next().unwrap();

    let points1 = parse_line2(line1);
    let points2 = parse_line2(line2);

    (points1, points2)
}

pub fn part1() -> String {
    do_p1(input::INPUT)
}

fn do_p1(question: &str) -> String {
    let (points1, points2) = parse_input(question);

    let mut closest: Option<i32> = None;
    for p in points1 {
        if p.x == 0 && p.y == 0 { continue; }
        if points2.contains(&p) {
            let distance = p.x.abs() + p.y.abs();
            if closest == None || distance < closest.unwrap() {
                closest = Some(distance);
            }
        }
    }

    if let Some(closest) = closest {
        format!("{}", closest)
    } else {
        "No crossing-point found!".to_string()
    }
}

fn do_p2(question: &str) -> String {
    let (points1, points2) = parse_input2(question);

    let mut closest: Option<i32> = None;
    for (pos, len1) in points1.iter() {
        if let Some(len2) = points2.get(pos) {
            let distance = len1 + len2;
            if closest == None || distance < closest.unwrap() {
                closest = Some(distance);
            }
        }
    }
    if let Some(closest) = closest {
        format!("{}", closest)
    } else {
        "No crossing-point found!".to_string()
    }
}

pub fn part2() -> String {
    do_p2(input::INPUT)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_d3_part2() {
        let test_data = vec![
            ("R8,U5,L5,D3\nU7,R6,D4,L4", "30"),
            ("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", "610"),
            ("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", "410"),
        ];

        for (q, a) in test_data {
            assert_eq!(a, do_p2(q));
        }
    }

    #[test]
    fn test_d3_part1() {
        let test_data = vec![
            ("R8,U5,L5,D3\nU7,R6,D4,L4", "6"),
            ("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83", "159"),
            ("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7", "135")
        ];

        for (q, a) in test_data {
            assert_eq!(a, do_p1(q));
        }
    }
}
