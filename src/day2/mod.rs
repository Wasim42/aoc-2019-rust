// Day 2 - Intcode

mod input;

use super::intcode;

pub fn part1() -> String {
    let mut machine = intcode::Machine::new(input::INPUT);
    machine.poke(1, 12);
    machine.poke(2, 2);
    machine.run();
    let answer = machine.peek(0).unwrap();
    format!("{}", answer)
}

pub fn part2() -> String {
    for noun in 0..100 {
        for verb in 0..100 {
            let mut machine = intcode::Machine::new(input::INPUT);
            machine.poke(1, noun);
            machine.poke(2, verb);
            machine.run();
            let answer = machine.peek(0).unwrap();
            if *answer == 19690720 {
                return format!("{}", noun*100 + verb);
            }
        }
    }
    "FAILED".to_string()
}

