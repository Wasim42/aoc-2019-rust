mod input;

use std::thread;

use itertools::Itertools;

use super::intcode;

fn get_biggest(prog: &str, looped: bool) -> i64 {
    let phases;

    if looped {
        phases = (5..10).permutations(5);
    } else {
        phases = (0..5).permutations(5);
    }

    let mut result = 0;

    for phase in phases {
        let answer = run_amplifiers(prog, phase, looped);
        if answer > result {
            result = answer;
        }
    }
    result
}

macro_rules! new_amp {
    ($prog:ident, $id:expr) => {
        {
            let mut amp = intcode::Machine::new($prog);
            amp.set_name(format!("Amp {}", $id));
            amp
        }
    }
}

fn run_amplifiers(prog: &str, mut phase: Vec<i64>, looped: bool) -> i64 {
    // Borrowing is a pain!
    let mut amp_a = new_amp!(prog, 'A');
    let mut amp_b = new_amp!(prog, 'B');
    let mut amp_c = new_amp!(prog, 'C');
    let mut amp_d = new_amp!(prog, 'D');
    let mut amp_e = new_amp!(prog, 'E');

    // Now get the connections set up…
    // First, we connect the outputs of the current to the inputs of the next
    // one
    amp_a.set_outputter(amp_b.get_inputter());
    amp_b.set_outputter(amp_c.get_inputter());
    amp_c.set_outputter(amp_d.get_inputter());
    amp_d.set_outputter(amp_e.get_inputter());
    if looped {
        // Loop all the way round!
        amp_e.set_outputter(amp_a.get_inputter());
    }

    // Now we need to prime the amps
    let _ = amp_a.get_inputter().send(phase.pop().unwrap());
    let _ = amp_b.get_inputter().send(phase.pop().unwrap());
    let _ = amp_c.get_inputter().send(phase.pop().unwrap());
    let _ = amp_d.get_inputter().send(phase.pop().unwrap());
    let _ = amp_e.get_inputter().send(phase.pop().unwrap());

    // We need to send an additional 0 to amp_a
    let _ = amp_a.get_inputter().send(0);

    // Now to set off the amps
    let _ = thread::spawn(move || {amp_a.run()});
    let _ = thread::spawn(move || {amp_b.run()});
    let _ = thread::spawn(move || {amp_c.run()});
    let _ = thread::spawn(move || {amp_d.run()});

    amp_e.run();
    amp_e.get_output()
}

pub fn part1() -> String {
    format!("{}", get_biggest(input::INPUT, false))
}

pub fn part2() -> String {
    format!("{}", get_biggest(input::INPUT, true))
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn check_biggest_simple_amps() {
        let test_data = [
            ("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0", 43210),
            ("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0", 54321),
            ("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0", 65210),
        ];

        for (prog, answer) in test_data.iter() {
            assert_eq!(answer.clone(), get_biggest(prog, false));
        }
    }

    #[test]
    fn check_biggest_looped_amps() {
        let test_data = [
            ("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5", 139629729),
            // ("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10", 18216),
        ];

        for (prog, answer) in test_data.iter() {
            assert_eq!(answer.clone(), get_biggest(prog, true));
        }
    }

}


