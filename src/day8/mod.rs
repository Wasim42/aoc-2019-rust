mod input;

struct Layers {
    width: u32,
    height: u32,
    layer_length: u32,
    data: Vec<u32>,
}

fn parse_string(msg: &str, size: (u32, u32)) -> Layers {
    Layers {
        width: size.0,
        height: size.1,
        layer_length: size.0 * size.1,
        data: msg.chars().map(|x| x.to_digit(10).unwrap()).collect()
    }
}

fn get_layer_with_fewest_1x2(number: u32, layers: &Layers) -> i32 {
    let mut smallest = layers.layer_length;
    let mut onextwo = 0;

    for layer in layers.data.chunks_exact(
        layers.layer_length as usize) {
        let count = layer.iter().fold(0, |acc, x| if x == &number { acc+ 1 } else {acc} );
        if count < smallest {
            smallest = count;
            onextwo = layer.iter().fold(0, |acc, x| if x == &1{acc+1}else{acc})
                * layer.iter().fold(0, |acc, x| if x == &2{acc+1}else{acc});
        }
    }

    onextwo

}

macro_rules! first_non_transparent {
    ($layers:ident, $x:ident, $y:ident) => {
        {
            let mut p: &u32 = &2;

            for l in 0..$layers.layer_length {
                p = $layers.data.get(
                    ($layers.layer_length * l
                    + $y*$layers.width + $x) as usize).unwrap();
                if p != &2 {
                    break;
                }
            }
            p
        }
    }
}
fn draw_pixels(layers: &Layers) -> String {
    let mut msg = String::new();
    for y in 0..layers.height {
        for x in 0..layers.width {
            let p = first_non_transparent!(layers, x, y);
            if p == &0 {
                msg.push(' ');
            } else {
                msg.push('X');
            }
        }
        msg.push('\n');
    }
    msg
}

fn solve_part1(msg: &str, size: (u32, u32)) -> i32 {

    let layers = parse_string(msg, size);
    get_layer_with_fewest_1x2(0, &layers)
}

fn solve_part2(msg: &str, size: (u32, u32)) -> String {

    let layers = parse_string(msg, size);
    draw_pixels(&layers)
}

pub fn part1() -> String {
    let answer = solve_part1(input::INPUT, (25, 6));
    format!("{}", answer)
}

pub fn part2() -> String {
    let answer = solve_part2(input::INPUT, (25, 6));
    format!("\n{}", answer)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let msg = "123456789012";
        let size = (3, 2);
        let answer = 1;

        assert_eq!(answer, solve_part1(msg, size));
    }

    #[test]
    fn test_part2() {
        let msg = "0222112222120000";
        let size = (2, 2);
        let answer = " X\nX \n";

        assert_eq!(answer, solve_part2(msg, size));
    }

}
