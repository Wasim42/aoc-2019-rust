mod input;

use super::intcode;

pub fn part1() -> String {
    let mut machine = intcode::Machine::new(input::INPUT);
    let inputter = machine.get_inputter();
    inputter.send(1).unwrap();
    machine.run();
    let mut answer;
    loop {
        answer = machine.get_output();
        if answer != 0 {
            break;
        }
    }
    format!("{}", answer)
}

pub fn part2() -> String {
    let mut machine = intcode::Machine::new(input::INPUT);
    let inputter = machine.get_inputter();
    inputter.send(5).unwrap();
    machine.run();
    format!("{}", machine.get_output())
}
